#/usr/bin/env bash

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

. ${THIS_DIR}/lib.sh

main() {
  sudo ln -fs ${THIS_DIR}/start.sh /usr/bin/${SERVICE}.start.sh
  sudo ln -fs ${THIS_DIR}/stop.sh /usr/bin/${SERVICE}.stop.sh

  sudo systemctl link ${THIS_DIR}/${SERVICE}.service

  sudo systemctl enable ${SERVICE}

  sudo systemctl start ${SERVICE}
}

main "$@"
