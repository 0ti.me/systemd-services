#/usr/bin/env bash

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

. ${THIS_DIR}/lib.sh

main() {
  sudo systemctl stop ${SERVICE}

  sudo systemctl disable ${SERVICE}

  sudo rm /usr/bin/${SERVICE}.start.sh
  sudo rm /usr/bin/${SERVICE}.stop.sh
}

main "$@"
