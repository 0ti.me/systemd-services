#/usr/bin/env bash

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

set -ex

main() {
  if cat /proc/acpi/wakeup | grep -E '^XHC1' | grep enabled; then
    sudo bash -c "echo XHC1 > /proc/acpi/wakeup"
  fi

  if cat /proc/acpi/wakeup | grep -E '^LID0' | grep enabled; then
    sudo bash -c "echo LID0 > /proc/acpi/wakeup"
  fi
}

main "$@"
