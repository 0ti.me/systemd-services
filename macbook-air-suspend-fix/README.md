# Macbook Air Suspend Fix

## Assumptions

* You have `bash` installed on your system.
* Your distribution of linux supports the use of `systemd` for managing startup/shutdown of services.

Try this to verify both:

    /usr/bin/env bash -c '[ -d /etc/systemd ] && echo "Supported" || echo "Not supported"' 2>/dev/null || echo "Not supported"

## Warnings

### Reduced Wakeup Functionality

This solution will prevent you from waking up the laptop using:

* USB devices like a keyboard or mouse
* The lid

## Steps

### Test that it works

Test with `./start.sh`.  Experiment with standby using whatever methods you expect to use.  Observe that the laptop properly holds in standby.

### Install the fix so it works after a reboot

Enable a service which will autostart on boot with `./enable.sh`.

## Steps to Remove

Uninstall with `./disable.sh`.

## Inspiration / Credit

https://old.reddit.com/r/Fedora/comments/66rwng/here_is_a_solution_to_the_problem_of_macbook_air/

### Credit

https://old.reddit.com/u/harold_admin
