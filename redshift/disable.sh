#/usr/bin/env bash

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

. ${THIS_DIR}/lib.sh

main() {
  systemctl --user stop ${SERVICE}

  systemctl --user disable ${SERVICE}

  sudo rm /usr/bin/${SERVICE}.start.sh
}

main "$@"
