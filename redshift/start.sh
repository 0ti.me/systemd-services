#!/usr/bin/env bash

ONESHOT=0

if [ ${ONESHOT} -eq 1 ]; then
  ONESHOT_ARG="-o"
fi

redshift \
  -l 45.4:-122.8 \
  -t 5700:3000 \
  -g 0.8 \
  -m randr \
  "${ONESHOT_ARG}" \
  -b 1.0:0.75 \
  -v
