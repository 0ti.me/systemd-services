#/usr/bin/env bash

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

. ${THIS_DIR}/lib.sh

main() {
  if ! which redshift >/dev/null 2>&1; then
    if which apt >/dev/null 2>&1; then
      sudo apt install redshift
    fi
  fi

  sudo ln -fs ${THIS_DIR}/start.sh /usr/bin/${SERVICE}.start.sh

  systemctl --user link ${THIS_DIR}/${SERVICE}.service

  systemctl --user enable ${SERVICE}

  systemctl --user start ${SERVICE}
}

main "$@"
